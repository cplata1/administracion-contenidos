[
  {
    "op": "core/column-reorder",
    "description": "Reorder columns",
    "columnNames": [
      "_ - results - _ - key",
      "_ - results - _ - doi",
      "_ - results - _ - type",
      "_ - results - _ - title",
      "_ - results - _ - created",
      "_ - results - _ - modified",
      "_ - results - _ - version",
      "_ - results - _ - publishingOrganizationKey"
    ]
  },
  {
    "op": "core/row-removal",
    "description": "Remove rows",
    "engineConfig": {
      "mode": "row-based",
      "facets": [
        {
          "omitError": false,
          "expression": "value",
          "selectBlank": true,
          "selection": [],
          "selectError": false,
          "invert": false,
          "name": "_ - results - _ - key",
          "omitBlank": false,
          "type": "list",
          "columnName": "_ - results - _ - key"
        }
      ]
    }
  },
  {
    "op": "core/column-rename",
    "description": "Rename column _ - results - _ - key to key",
    "oldColumnName": "_ - results - _ - key",
    "newColumnName": "key"
  },
  {
    "op": "core/column-rename",
    "description": "Rename column _ - results - _ - doi to doi",
    "oldColumnName": "_ - results - _ - doi",
    "newColumnName": "doi"
  },
  {
    "op": "core/column-rename",
    "description": "Rename column _ - results - _ - type to type",
    "oldColumnName": "_ - results - _ - type",
    "newColumnName": "type"
  },
  {
    "op": "core/column-rename",
    "description": "Rename column _ - results - _ - title to title",
    "oldColumnName": "_ - results - _ - title",
    "newColumnName": "title"
  },
  {
    "op": "core/column-rename",
    "description": "Rename column _ - results - _ - created to created",
    "oldColumnName": "_ - results - _ - created",
    "newColumnName": "created"
  },
  {
    "op": "core/column-rename",
    "description": "Rename column _ - results - _ - modified to modified",
    "oldColumnName": "_ - results - _ - modified",
    "newColumnName": "modified"
  },
  {
    "op": "core/column-rename",
    "description": "Rename column _ - results - _ - version to version",
    "oldColumnName": "_ - results - _ - version",
    "newColumnName": "version"
  },
  {
    "op": "core/column-rename",
    "description": "Rename column _ - results - _ - publishingOrganizationKey to publishingOrganizationKey",
    "oldColumnName": "_ - results - _ - publishingOrganizationKey",
    "newColumnName": "publishingOrganizationKey"
  },
  {
    "op": "core/column-split",
    "description": "Split column created by separator",
    "engineConfig": {
      "mode": "row-based",
      "facets": []
    },
    "columnName": "created",
    "guessCellType": false,
    "removeOriginalColumn": false,
    "mode": "separator",
    "separator": "T",
    "regex": false,
    "maxColumns": 0
  },
  {
    "op": "core/column-removal",
    "description": "Remove column created",
    "columnName": "created"
  },
  {
    "op": "core/column-removal",
    "description": "Remove column created 2",
    "columnName": "created 2"
  },
  {
    "op": "core/column-rename",
    "description": "Rename column created 1 to created",
    "oldColumnName": "created 1",
    "newColumnName": "created"
  },
  {
    "op": "core/column-split",
    "description": "Split column created by separator",
    "engineConfig": {
      "mode": "row-based",
      "facets": []
    },
    "columnName": "created",
    "guessCellType": false,
    "removeOriginalColumn": false,
    "mode": "separator",
    "separator": "-",
    "regex": false,
    "maxColumns": 3
  },
  {
    "op": "core/column-rename",
    "description": "Rename column created 1 to year",
    "oldColumnName": "created 1",
    "newColumnName": "year"
  },
  {
    "op": "core/column-rename",
    "description": "Rename column created 3 to day",
    "oldColumnName": "created 3",
    "newColumnName": "day"
  },
  {
    "op": "core/column-rename",
    "description": "Rename column created 2 to month",
    "oldColumnName": "created 2",
    "newColumnName": "month"
  },
  {
    "op": "core/column-split",
    "description": "Split column modified by separator",
    "engineConfig": {
      "mode": "row-based",
      "facets": []
    },
    "columnName": "modified",
    "guessCellType": false,
    "removeOriginalColumn": false,
    "mode": "separator",
    "separator": "T",
    "regex": false,
    "maxColumns": 0
  },
  {
    "op": "core/column-removal",
    "description": "Remove column modified",
    "columnName": "modified"
  },
  {
    "op": "core/column-removal",
    "description": "Remove column modified 2",
    "columnName": "modified 2"
  },
  {
    "op": "core/column-rename",
    "description": "Rename column modified 1 to modified",
    "oldColumnName": "modified 1",
    "newColumnName": "modified"
  },
  {
    "op": "core/column-split",
    "description": "Split column modified by separator",
    "engineConfig": {
      "mode": "row-based",
      "facets": []
    },
    "columnName": "modified",
    "guessCellType": false,
    "removeOriginalColumn": false,
    "mode": "separator",
    "separator": "-",
    "regex": false,
    "maxColumns": 3
  },
  
  {
    "op": "core/column-rename",
    "description": "Rename column modified 1 to year",
    "oldColumnName": "modified 1",
    "newColumnName": "year-mod"
  },
  {
    "op": "core/column-rename",
    "description": "Rename column modified 3 to day",
    "oldColumnName": "modified 3",
    "newColumnName": "day-mod"
  },
  {
    "op": "core/column-rename",
    "description": "Rename column modified 2 to month",
    "oldColumnName": "modified 2",
    "newColumnName": "month-mod"
  },
  {
    "op": "core/text-transform",
    "description": "Text transform on cells in column doi using expression grel:\"http://doi.org/\"+value",
    "engineConfig": {
      "mode": "row-based",
      "facets": []
    },
    "columnName": "doi",
    "expression": "grel:\"http://doi.org/\"+value",
    "onError": "keep-original",
    "repeat": false,
    "repeatCount": 10
  },
  {
    "op": "core/column-rename",
    "description": "Rename column doi to DOI_URL",
    "oldColumnName": "doi",
    "newColumnName": "DOI_URL"
  },
  {
    "op": "core/column-move",
    "description": "Move column DOI_URL to position 3",
    "columnName": "DOI_URL",
    "index": 3
  },
  {
    "op": "core/text-transform",
    "description": "Text transform on cells in column type using expression grel:value.replace(\"OCCURRENCE\",\"Registros biológicos\").replace(\"CHECKLIST\",\"Listas de especies\").replace(\"METADATA\",\"Metadatos\").replace(\"SAMPLING_EVENT\",\"Eventos de muestreo\")",
    "engineConfig": {
      "mode": "row-based",
      "facets": []
    },
    "columnName": "type",
    "expression": "grel:value.replace(\"OCCURRENCE\",\"Registros biológicos\").replace(\"CHECKLIST\",\"Listas de especies\").replace(\"METADATA\",\"Metadatos\").replace(\"SAMPLING_EVENT\",\"Eventos de muestreo\")",
    "onError": "keep-original",
    "repeat": false,
    "repeatCount": 10
  },
  {
    "op": "core/column-addition-by-fetching-urls",
    "description": "Create column apiCALL at index 2 by fetching URLs based on column publishingOrganizationKey using expression grel:\"http://api.gbif.org/v1/organization/\"+value",
    "engineConfig": {
      "mode": "row-based",
      "facets": []
    },
    "newColumnName": "apiCALL",
    "columnInsertIndex": 2,
    "baseColumnName": "publishingOrganizationKey",
    "urlExpression": "grel:\"http://api.gbif.org/v1/organization/\"+value",
    "onError": "set-to-blank",
    "delay": 5,
    "cacheResponses": true,
    "httpHeadersJson": [
      {
        "name": "authorization",
        "value": ""
      },
      {
        "name": "user-agent",
        "value": "OpenRefine 3.0 [TRUNK]"
      },
      {
        "name": "accept",
        "value": "*/*"
      }
    ]
  },
  {
    "op": "core/column-addition",
    "description": "Create column organization at index 3 based on column apiCALL using expression grel:value.parseJson().get(\"title\")",
    "engineConfig": {
      "mode": "row-based",
      "facets": []
    },
    "newColumnName": "organization",
    "columnInsertIndex": 3,
    "baseColumnName": "apiCALL",
    "expression": "grel:value.parseJson().get(\"title\")",
    "onError": "set-to-blank"
  },
  {
    "op": "core/column-removal",
    "description": "Remove column apiCALL",
    "columnName": "apiCALL"
  },
  {
    "op": "core/column-addition-by-fetching-urls",
    "description": "Create column apiCall at index 1 by fetching URLs based on column key using expression grel:\"http://api.gbif.org/v1/dataset/\"+value",
    "engineConfig": {
      "mode": "row-based",
      "facets": []
    },
    "newColumnName": "apiCall",
    "columnInsertIndex": 1,
    "baseColumnName": "key",
    "urlExpression": "grel:\"http://api.gbif.org/v1/dataset/\"+value",
    "onError": "set-to-blank",
    "delay": 5,
    "cacheResponses": true,
    "httpHeadersJson": [
      {
        "name": "authorization",
        "value": ""
      },
      {
        "name": "user-agent",
        "value": "OpenRefine 3.0 [TRUNK]"
      },
      {
        "name": "accept",
        "value": "*/*"
      }
    ]
  },
  {
    "op": "core/column-addition",
    "description": "Create column nombrecorto at index 2 based on column apiCall using expression grel:value.parseJson().get(\"endpoints\")[1].get(\"url\").replace(\"https://ipt.biodiversidad.co/sib/eml.do?r=\",\"\")",
    "engineConfig": {
      "mode": "row-based",
      "facets": []
    },
    "newColumnName": "nombrecorto",
    "columnInsertIndex": 2,
    "baseColumnName": "apiCall",
    "expression": "grel:value.parseJson().get(\"endpoints\")[1].get(\"url\").replace(\"https://ipt.biodiversidad.co/sib/eml.do?r=\",\"\")",
    "onError": "set-to-blank"
  },
  {
    "op": "core/text-transform",
    "description": "Text transform on cells in column nombrecorto using expression grel:value.replace(\"https://ipt.biodiversidad.co/iiap/eml.do?r=\",\"\").replace(\"https://ipt.biodiversidad.co/iavh/eml.do?r=\",\"\").replace(\"https://ipt.biodiversidad.co/sinchi/eml.do?r=\",\"\").replace(\"https://ipt.biodiversidad.co/parquesnacionales/eml.do?r=\",\"\").replace(\"https://ipt.biodiversidad.co/sibm/eml.do?r=\",\"\").replace(\"https://ipt.biodiversidad.co/biota/eml.do?r=\",\"\")",
    "engineConfig": {
      "mode": "row-based",
      "facets": [
        {
          "mode": "text",
          "invert": false,
          "caseSensitive": false,
          "query": "http",
          "name": "nombrecorto",
          "type": "text",
          "columnName": "nombrecorto"
        }
      ]
    },
    "columnName": "nombrecorto",
    "expression": "grel:value.replace(\"https://ipt.biodiversidad.co/iiap/eml.do?r=\",\"\").replace(\"https://ipt.biodiversidad.co/iavh/eml.do?r=\",\"\").replace(\"https://ipt.biodiversidad.co/sinchi/eml.do?r=\",\"\").replace(\"https://ipt.biodiversidad.co/parquesnacionales/eml.do?r=\",\"\").replace(\"https://ipt.biodiversidad.co/sibm/eml.do?r=\",\"\").replace(\"https://ipt.biodiversidad.co/biota/eml.do?r=\",\"\")",
    "onError": "keep-original",
    "repeat": false,
    "repeatCount": 10
  },
  {
    "op": "core/text-transform",
    "description": "Text transform on cells in column nombrecorto using expression grel:value.replace(\"http://ipt.biodiversidad.co/iiap/eml.do?r=\",\"\").replace(\"http://ipt.biodiversidad.co/iavh/eml.do?r=\",\"\").replace(\"http://ipt.biodiversidad.co/sinchi/eml.do?r=\",\"\").replace(\"http://ipt.biodiversidad.co/parquesnacionales/eml.do?r=\",\"\").replace(\"http://ipt.biodiversidad.co/sibm/eml.do?r=\",\"\").replace(\"http://ipt.biodiversidad.co/biota/eml.do?r=\",\"\").replace(\"http://ipt.biodiversidad.co/sib/eml.do?r=\",\"\")",
    "engineConfig": {
      "mode": "row-based",
      "facets": [
        {
          "mode": "text",
          "invert": false,
          "caseSensitive": false,
          "query": "http",
          "name": "nombrecorto",
          "type": "text",
          "columnName": "nombrecorto"
        }
      ]
    },
    "columnName": "nombrecorto",
    "expression": "grel:value.replace(\"http://ipt.biodiversidad.co/iiap/eml.do?r=\",\"\").replace(\"http://ipt.biodiversidad.co/iavh/eml.do?r=\",\"\").replace(\"http://ipt.biodiversidad.co/sinchi/eml.do?r=\",\"\").replace(\"http://ipt.biodiversidad.co/parquesnacionales/eml.do?r=\",\"\").replace(\"http://ipt.biodiversidad.co/sibm/eml.do?r=\",\"\").replace(\"http://ipt.biodiversidad.co/biota/eml.do?r=\",\"\").replace(\"http://ipt.biodiversidad.co/sib/eml.do?r=\",\"\")",
    "onError": "keep-original",
    "repeat": false,
    "repeatCount": 10
  },
  {
    "op": "core/mass-edit",
    "description": "Mass edit cells in column nombrecorto",
    "engineConfig": {
      "mode": "row-based",
      "facets": [
        {
          "mode": "text",
          "invert": false,
          "caseSensitive": false,
          "query": "http",
          "name": "nombrecorto",
          "type": "text",
          "columnName": "nombrecorto"
        }
      ]
    },
    "columnName": "nombrecorto",
    "expression": "value",
    "edits": [
      {
        "fromBlank": false,
        "fromError": false,
        "from": [
          "http://ipt.parquesnacionales.gov.co/eml.do?r=fauna_y_flora_de_cinaruco_2014"
        ],
        "to": "fauna_y_flora_de_cinaruco_2014"
      }
    ]
  },
  {
    "op": "core/mass-edit",
    "description": "Mass edit cells in column nombrecorto",
    "engineConfig": {
      "mode": "row-based",
      "facets": [
        {
          "mode": "text",
          "invert": false,
          "caseSensitive": false,
          "query": "http",
          "name": "nombrecorto",
          "type": "text",
          "columnName": "nombrecorto"
        }
      ]
    },
    "columnName": "nombrecorto",
    "expression": "value",
    "edits": [
      {
        "fromBlank": false,
        "fromError": false,
        "from": [
          "http://ipt.sibcolombia.net/rnoa/eml.do?r=abo-002"
        ],
        "to": "abo-002"
      }
    ]
  },
  {
    "op": "core/mass-edit",
    "description": "Mass edit cells in column nombrecorto",
    "engineConfig": {
      "mode": "row-based",
      "facets": [
        {
          "mode": "text",
          "invert": false,
          "caseSensitive": false,
          "query": "http",
          "name": "nombrecorto",
          "type": "text",
          "columnName": "nombrecorto"
        }
      ]
    },
    "columnName": "nombrecorto",
    "expression": "value",
    "edits": [
      {
        "fromBlank": false,
        "fromError": false,
        "from": [
          "http://ipt.biodiversidad.co/resnatur/eml.do?r=aves_rn_elparaiso"
        ],
        "to": "aves_rn_elparaiso"
      }
    ]
  },
  {
    "op": "core/mass-edit",
    "description": "Mass edit cells in column nombrecorto",
    "engineConfig": {
      "mode": "row-based",
      "facets": [
        {
          "mode": "text",
          "invert": false,
          "caseSensitive": false,
          "query": "http",
          "name": "nombrecorto",
          "type": "text",
          "columnName": "nombrecorto"
        }
      ]
    },
    "columnName": "nombrecorto",
    "expression": "value",
    "edits": [
      {
        "fromBlank": false,
        "fromError": false,
        "from": [
          "http://ipt.biodiversidad.co/sirap-ec/eml.do?r=biodiversidad_pnr_verdum"
        ],
        "to": "biodiversidad_pnr_verdum"
      }
    ]
  },
  {
    "op": "core/mass-edit",
    "description": "Mass edit cells in column nombrecorto",
    "engineConfig": {
      "mode": "row-based",
      "facets": [
        {
          "mode": "text",
          "invert": false,
          "caseSensitive": false,
          "query": "http",
          "name": "nombrecorto",
          "type": "text",
          "columnName": "nombrecorto"
        }
      ]
    },
    "columnName": "nombrecorto",
    "expression": "value",
    "edits": [
      {
        "fromBlank": false,
        "fromError": false,
        "from": [
          "http://ipt.biodiversidad.co/valle/eml.do?r=hormigasbordebosqueseco"
        ],
        "to": "hormigasbordebosqueseco"
      }
    ]
  },
  {
    "op": "core/mass-edit",
    "description": "Mass edit cells in column nombrecorto",
    "engineConfig": {
      "mode": "row-based",
      "facets": [
        {
          "mode": "text",
          "invert": false,
          "caseSensitive": false,
          "query": "http",
          "name": "nombrecorto",
          "type": "text",
          "columnName": "nombrecorto"
        }
      ]
    },
    "columnName": "nombrecorto",
    "expression": "value",
    "edits": [
      {
        "fromBlank": false,
        "fromError": false,
        "from": [
          "https://data.gbif.no/ipt/eml.do?r=cwr"
        ],
        "to": "cwr"
      }
    ]
  },
  {
    "op": "core/mass-edit",
    "description": "Mass edit cells in column nombrecorto",
    "engineConfig": {
      "mode": "row-based",
      "facets": [
        {
          "mode": "text",
          "invert": false,
          "caseSensitive": false,
          "query": "http",
          "name": "nombrecorto",
          "type": "text",
          "columnName": "nombrecorto"
        }
      ]
    },
    "columnName": "nombrecorto",
    "expression": "value",
    "edits": [
      {
        "fromBlank": false,
        "fromError": false,
        "from": [
          "https://pruebasipt.biodiversidad.co/sibm/eml.do?r=peces_unidades_ecologicas_someras_serranilla2017-proyecto_colombia_bio"
        ],
        "to": "peces_unidades_ecologicas_someras_serranilla2017-proyecto_colombia_bio"
      }
    ]
  },
  {
    "op": "core/column-removal",
    "description": "Remove column apiCall",
    "columnName": "apiCall"
  }
]