# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 15:09:04 2019

"""

import pandas as pd
import numpy  as np
import shapely as shply
import geopandas as gpd
import fiona
from shapely.geometry import Point
import matplotlib.pyplot as plt
import warnings
import rtree
import csv


# dwti=download toponim interpreted
dwti = pd.read_table('C:\\Cifras_Boyaca\\2018\\GBIFdata20181001\\ConsultaA_Text\\csv_interpretado.csv',encoding = "latin-1")
# dwto=download toponim occurrece-DwCA
dwto = pd.read_table('C:\\Cifras_Boyaca\\2018\\Descargas_GBIF\\Texto\\occurrence2.txt', encoding = "utf8")

# dwpi=download polygon interpreted
dwpi = pd.read_table('C:\\Cifras_Boyaca\\2018\\Descargas_GBIF\\Poligono\\Boyaca_poligono.csv',encoding = "utf8")
# dwpo=download polygon occurrece-DwCA
dwpo = pd.read_table('C:\\Cifras_Boyaca\\2018\\Descargas_GBIF\\Poligono\\occurrence2.txt',encoding = "utf8")

#remove eBird from datasets
dwti=dwti[dwti.datasetKey != '4fa7b334-ce0d-4e88-aaae-2e0c138d049e']
dwto=dwto[dwto.datasetKey != '4fa7b334-ce0d-4e88-aaae-2e0c138d049e']
dwpi=dwpi[dwpi.datasetKey != '4fa7b334-ce0d-4e88-aaae-2e0c138d049e']
dwpo=dwpo[dwpo.datasetKey != '4fa7b334-ce0d-4e88-aaae-2e0c138d049e']

#Refinamientos polígonos

#2.Crear un elemento nuevo 'Coordinates', con las coordenadas organizadas en un tupla
dwpi['Coordinates'] = list(zip(dwpi.decimalLongitude, dwpi.decimalLatitude))
dwpo['Coordinates'] = list(zip(dwpo.decimalLongitude, dwpo.decimalLatitude))

#3.Transformar la tupla en un elemento geométrico tipo punto
dwpi['Coordinates'] = dwpi['Coordinates'].apply(Point)
dwpo['Coordinates'] = dwpo['Coordinates'].apply(Point)


#4.Crear el geodataframe asignando el elemento que contiene la geometría
dwpi_g = gpd.GeoDataFrame(dwpi, geometry='Coordinates')
dwpo_g = gpd.GeoDataFrame(dwpo, geometry='Coordinates')

#5. Asignar el sistema de referencia al geodataframe
dwpi_g.crs = {'init' :'epsg:4326'}
dwpo_g.crs = {'init' :'epsg:4326'}

#6.Importar archivo shapefile (revisar la codificación de la capa y el sistema de referencia) preferiblemente que esté ya en WGS84, sino hay que realizar una conversión para que funcione el spatialJoin
BoyacaShp =gpd.read_file('C:\\Cifras_Boyaca\\Capas\\Boyaca_mpios.shp', encoding = "utf8")


# Unión espacial entre el geodataframe y el shapefile (primero el geodataframe, shapefile)
dwpi_gsj = gpd.sjoin(dwpi_g, BoyacaShp, how="inner", op= "intersects")
dwpo_gsj = gpd.sjoin(dwpo_g, BoyacaShp, how="inner", op="intersects")

# load eBird repatriated data
dwebird = pd.read_table('C:\\Cifras_Boyaca\\2018\\Repatriados\\eBird\\ebd_boyaca.txt')
dwebird.rename(columns={'occurrenceID': 'gbifID'}, inplace=True)
dwebird_add = pd.read_table('C:\\Cifras_Boyaca\\2018\\Repatriados\\eBird\\ebd_boyaca_add.txt')
dweb=pd.merge(dwebird,dwebird_add,on='gbifID')
dweb_s=dweb[["gbifID","datasetKey","publishingOrgKey","kingdom","phylum","class","order","family",
             "genus","species","taxonRank","scientificName","day","month","year",
             "decimalLatitude","decimalLongitude","locality","countryCode","stateProvince"]]


#References checkList
#Cites Cleaned species matching
cites = pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\CITES_speciesMatchCleaned_v20190124.txt')
# Amenazadas Cleaned species matching
threat = pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\AmenazadasMADS_speciesMatchCleaned_v20190124.txt')
# Endemic Plantas Cleaned 
plantEM= pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\PlantasEndemicas_v20190124.txt')
# Endemic  Fungi / Lichens Cleaned 
lichenEM= pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\HongosEndemicas_v20190124.txt')
#Endemic Birds Cleaned 
birdsEM= pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\AvesEndemicas_v20190124.txt')
#Endemic Birds Cleaned 
freshfEM= pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\PecesDulceEndemicas_v20190124.txt')
#Endemic Birds Cleaned 
mammalEM= pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\MamiferosEndemicas_v20190124.txt')
#Migratory birds
birdsMG= pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\Datos_AvesMigratorias_v20190131.txt')
#Exotic fauna
faunaEX= pd.read_table('C:\\Cifras_Boyaca\\2018\\Listas\\Datos_FaunaExotica_v20190131.txt')





# concatenate (add rows) both interpreted downloads
dwi=pd.concat([dwti,dwpi_gsj], sort = False)
#concatenate (add rows) both occurrece-DwCA
dwo=pd.concat([dwto,dwpo_gsj], sort = False)
#concatenate (add rows) all endemic files
em=pd.concat([plantEM, lichenEM, birdsEM, freshfEM, mammalEM],sort = False)


# remove duplicates between interpreted downloads
dwiu=dwi.drop_duplicates('gbifID',inplace=False)
# remove duplicates between occurrece-DwCA downloads
dwou=dwo.drop_duplicates('gbifID',inplace=False)

#+ebird duplicates? or rather remove if yep!!!!!

#keep only selected (usefull) elements
dwiu_s=dwiu[["gbifID","datasetKey","publishingOrgKey","kingdom","phylum","class","order","family",
             "genus","species","taxonRank","scientificName","day","month","year",
             "decimalLatitude","decimalLongitude","locality"]]
dwou_s=dwou[["gbifID","waterBody","island","countryCode","stateProvince","county","municipality"]]


cites_s=cites[["species","appendixCITES"]]
threat_s=threat[["species","threatStatus"]]
em_s=em[["species","establishmentMeans"]]
mi_s=birdsMG[["species","migratory"]]
ex_s=faunaEX[["species","exotic"]]



# working elements in a single file
dwm=pd.merge(dwiu_s,dwou_s)

#Homogenization of stateProvince toponim
dwm['stateProvince'] = dwm['stateProvince'].replace(["boyaca", "Boyaca","boyacá", "boy","Boy", "boyac", "Boyac", "boyac??", "Boyaca Department", 
   "Boyacá Department", "Boyaca Dept.", "boyac�","Boyac�", "CO-BOY", "Departamento de Boyaca", "Departamento de Boyacá", 
   "Dept. Boyaca", "Dpto. Boyacá", "Departamento de Boyac", "Dep. Boyacá", "Departamento Boyaca"], 'Boyacá')


# concatenate (add rows) working elements with eBird 
dwme=pd.concat([dwm,dweb_s], sort=False)



#Geographic spatial join. Get stateProvince and county according to cartograhpy 1:25000

#Fill empty coordinates with 0's
dwme[['decimalLatitude', 'decimalLongitude']] = dwme[['decimalLatitude','decimalLongitude']].fillna(value=0)
dwme['Coordinates'] = list(zip(dwme.decimalLongitude, dwme.decimalLatitude))
dwme['Coordinates'] = dwme['Coordinates'].apply(Point)
dwme_g = gpd.GeoDataFrame(dwme, geometry='Coordinates')
dwme_g.crs = {'init' :'epsg:4326'}
dwme_g = gpd.sjoin(dwme_g, BoyacaShp, how="left")
dwme_g = dwme_g.drop(['Area', 'index_right', 'gid'], axis=1)


#Cross main dataframe with thematic information
dwme_t=dwme_g.merge(cites_s,on='species',how='left')
dwme_t=dwme_t.merge(threat_s,on='species',how='left')
dwme_t=dwme_t.merge(em_s,on='species',how='left')
dwme_t=dwme_t.merge(mi_s,on='species',how='left')
dwme_t=dwme_t.merge(ex_s,on='species',how='left')




#Export the result
dwme_t.to_csv('C:\\Cifras_Boyaca\\2018\\dwme_t.csv', sep=",", encoding = "utf8")
nespecies = dwme_t.groupby(['species']).species.nunique().count()


