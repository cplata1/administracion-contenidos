# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 18:54:00 2019

@author: ricardo.ortiz
"""

import pandas as pd
import geopandas as gpd
from shapely.geometry import Point
import json
import time
import requests
from pandas.io.json import json_normalize



# Load  interpreted
dwi = pd.read_table("C:\\Cifras_Colombia\\2019\\2019-IV\\GBIF_Download\\Interpretado\\0032613-191105090559680.csv", encoding = "utf8", sep= "\t", quoting=3, 
                    usecols = ['gbifID', 'datasetKey', 'occurrenceID', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species', 'infraspecificEpithet', 'taxonRank', 'scientificName', 'verbatimScientificName', 'verbatimScientificNameAuthorship', 'countryCode', 'individualCount', 'publishingOrgKey', 'decimalLatitude', 'decimalLongitude', 'coordinateUncertaintyInMeters', 'elevation', 'depth', 'eventDate', 'day', 'month', 'year', 'taxonKey', 'basisOfRecord', 'institutionCode', 'collectionCode', 'catalogNumber', 'identifiedBy', 'dateIdentified', 'recordedBy'])

# Load occurrece-DwCA
dwo = pd.read_table("C:\\Cifras_Colombia\\2019\\2019-IV\\GBIF_Download\\Occurrence\\occurrence.txt", encoding = "utf8", sep= "\t", quoting=3,
                    usecols = ['gbifID', 'type','habitat', 'sex', 'samplingProtocol', 'samplingEffort', 'eventID','continent', 'waterBody','stateProvince', 'county', 'municipality', 'locality', 'publishingCountry', 'repatriated'])



#merge dwi dwo
dwm=pd.merge(dwi,dwo,on='gbifID')
#remove eBird from datasets
dwm=dwm[dwm.datasetKey != '4fa7b334-ce0d-4e88-aaae-2e0c138d049e']
#remove Xeno-Canto from dataset
dwm=dwm[dwm.datasetKey != 'b1047888-ae52-4179-9dd5-5448ea342a24']
#remove iNat from dataset
dwm=dwm[dwm.datasetKey != '50c9509d-22c7-4a22-a47d-8c48425ef4a7']
#remove repatriated from datasets
dwm=dwm[dwm.publishingCountry == 'CO']


#STOP... Remove the dwi and dwo variables from the explorer


# Add necesary elements to the dataset
dwm['institutionID'] = 'https://www.gbif.org/publisher/' + dwm['publishingOrgKey']
dwm['bibliographicCitation'] = 'https://www.gbif.org/occurrence/' + dwm['gbifID'].astype(str)
dwm['datasetID'] = 'https://www.gbif.org/dataset/' + dwm['datasetKey'].astype(str)
dwm['geodeticDatum']='forced to WGS84'
dwm['taxonID'] = 'https://www.gbif.org/species/' + dwm['taxonKey'].astype(str)
dwm['continent']=dwm['continent'].fillna('SA')
dwm['country']='Colombia'
#dwm['verbatimLocality']=dwr['continent'].astype(str)+ ' | '+dwr['countryCode'].astype(str)+ ' | '+dwr['stateProvince'].astype(str)+ ' | '+dwr['county'].astype(str)+ ' | '+dwr['municipality'].astype(str)+ ' | '+dwr['locality'].astype(str)
dwm['previousIdentifications']=dwm['verbatimScientificName']
dwm['minimumElevationInMeters']=dwm['elevation']
dwm['maximumElevationInMeters']=dwm['elevation']
dwm['minimumDepthInMeters']=dwm['depth']
dwm['maximumDepthInMeters']=dwm['depth']
dwm['occurrenceRemarks']='País publicador: '+dwm['publishingCountry']
#Split the species element to various elements. The second split (species 2) is taken as specificEpithet (Verify!!!!)
dwm['species1'],dwm['species2'],dwm['species3'],dwm['species4'] = dwm['species'].str.split(' ', 4).str
dwm = dwm.drop(['species1','species3','species4'], axis=1)
dwm.rename(columns={'species2': 'specificEpithet'}, inplace=True)
#Rename scientificName (gbif interpreted with authorship) as acceptedNameUsage
dwm.rename(columns={'scientificName': 'acceptedNameUsage'}, inplace=True)
#Change values according to controled vocabulary
dwm['basisOfRecord']=dwm['basisOfRecord'].replace('HUMAN_OBSERVATION', 'HumanObservation').replace('PRESERVED_SPECIMEN', 'PreservedSpecimen').replace('MACHINE_OBSERVATION', 'MachineObservation').replace('FOSSIL_SPECIMEN', 'FossilSpecimen').replace('MATERIAL_SAMPLE', 'MaterialSample').replace('LIVING_SPECIMEN', 'LivingSpecimen').replace('OBSERVATION', 'HumanObservation').replace('LITERATURE', 'Occurrence').replace('UNKNOWN', 'Occurrence')
dwm['taxonRank']=dwm['taxonRank'].replace('SPECIES', 'Especie').replace('SUBSPECIES', 'Subespecie').replace('GENUS', 'Género').replace('FAMILY', 'Familia').replace('ORDER', 'Orden').replace('CLASS', 'Clase').replace('PHYLUM', 'Filo').replace('KINGDOM', 'Reino').replace('FORM', 'Forma').replace('VARIETY', 'Variedad').replace('UNRANKED', '')
#Standarize and translate the Type element. Verify the unique values in Type in case you need to add more lines to the scipt ;)
dwm['type'].unique() # See the results in console
#Make voabulary changes in type element
dwm['type']=dwm['type'].replace('PhysicalObject', 'Objeto físico').replace('Image', 'Imagen estática').replace('Event', 'Evento').replace('Physical Object', 'Objeto físico').replace('collection', 'Objeto físico').replace('Collection', 'Objeto físico').replace('Sound', 'Sonido').replace('text', 'Texto').replace('Text', 'Texto').replace('StillImage', 'Imagen estática').replace('http://purl.org/dc/dcmitype/PhysicalObject', 'Objeto físico').replace('http://purl.org/dc/dcmitype/StillImage', 'Imagen estática').replace('Dataset', '').replace('event', 'Evento').replace('Colletion', 'Objeto físico').replace('Physical object', 'Objeto físico').replace('physicalObject', 'Objeto físico').replace('Preserved Specimen [en]', 'Objeto físico').replace('physical object', 'Objeto físico').replace('"Evento; Sonido"', 'Sonido').replace('"Evento;Sonido"', 'Sonido').replace('"Sonido;Evento"', 'Sonido').replace('nan', '').replace('Objeto Fisico', 'Objeto físico').replace('Imagen Estática', 'Imagen estática').replace('evento', 'Evento').replace('Imagén estática', 'Imagen estática').replace('Evento; Sonido', 'Sonido').replace('Imagen movimiento', 'Imagen en movimiento').replace('MovingImage', 'Imagen en movimiento').replace('Imagen estatica', 'Imagen estática').replace('Imajen fija', 'Imagen estática').replace('Objeto f’sico', 'Objeto físico').replace('Imágen', 'Imagen').replace('Grabación (track 40)', 'Sonido').replace('Grabación (track 52)', 'Sonido').replace('Grabación (track 46)', 'Sonido').replace('Grabación (track 54)', 'Sonido').replace('Grabación (track 45, 48,49,51)', 'Sonido').replace('Grabación (track 43)', 'Sonido').replace('es', '').replace('Vocalización', 'Evento').replace('Avistamiento/Vocalización', 'Evento').replace('Avistamiento', 'Evento').replace('Especimen preservado', 'Objeto físico').replace('Objeto Físico', 'Objeto físico').replace('Objeto fisico', 'Objeto físico').replace('SonidoObjeto físico', 'Objeto físico')
#Create a scientificName element without Authorship based in the taxonRank value
dwm.loc[dwm.taxonRank == 'Reino', 'scientificName'] = dwm['kingdom']
dwm.loc[dwm.taxonRank == 'Filo', 'scientificName'] = dwm['phylum']
dwm.loc[dwm.taxonRank == 'Clase', 'scientificName'] = dwm['class']
dwm.loc[dwm.taxonRank == 'Orden', 'scientificName'] = dwm['order']
dwm.loc[dwm.taxonRank == 'Familia', 'scientificName'] = dwm['family']
dwm.loc[dwm.taxonRank == 'Género', 'scientificName'] = dwm['genus']
dwm.loc[dwm.taxonRank == 'Especie', 'scientificName'] = dwm['species'] # Carefull, some times the species could be empty... GBIF issue. Needs to be checked out of the script 
dwm.loc[dwm.taxonRank == 'Subespecie', 'scientificName'] = dwm['species']+' '+dwm['infraspecificEpithet']
dwm.loc[dwm.taxonRank == 'Variedad', 'scientificName'] = dwm['species']+' var. '+dwm['infraspecificEpithet']
dwm.loc[dwm.taxonRank == 'Forma', 'scientificName'] = dwm['species']+' f. '+dwm['infraspecificEpithet']
#Keep species element like a taxonRemark
dwm.rename(columns={'species': 'taxonRemarks'}, inplace=True)
#Organize date
dwm['eventDate'],dwm['ToDrop']= dwm['eventDate'].str.split('T', 2).str

#Remove innecesary columns
dwm = dwm.drop(['ToDrop', 'verbatimScientificName', 'verbatimScientificNameAuthorship',  'elevation', 'depth',  'publishingCountry', 'repatriated'], axis=1)




# Load eBird repatriated data
dwebird = pd.read_table('C:\\Cifras_Colombia\\2019\\Repatriados\\eBird\\occurrence.txt')
#Add gbifID. Carefull, only apply if it comes from the ipt Occurrence, otherwise, use 
dwebird.rename(columns={'id': 'gbifID'}, inplace=True)
dwebird['bibliographicCitation'] = 'https://www.gbif.org/occurrence/' + dwebird['gbifID'].astype(str)
dwebird['institutionCode']='eBird Colombia'
dwebird['continent']='SA'
dwebird['geodeticDatum']='forced to WGS84'
dwebird["taxonRemarks"]=dwebird["scientificName"]#Crear la columna taxonRemarks para documentar el species como duplicado de la columna scientificName
dwebird['previousIdentifications'] = dwebird['scientificNameAuthorship']#Mejora: Unificar el formato del nombre científico uniendo la autoría como está en dwi
dwebird['datasetKey'] = '4fa7b334-ce0d-4e88-aaae-2e0c138d049e'
dwebird['datasetID'] = 'https://www.gbif.org/dataset/' + dwebird['datasetKey'].astype(str)
dwebird['publishingOrgKey'] = 'e2e717bf-551a-4917-bdc9-4fa0f342c530'
dwebird['institutionID'] = 'https://www.gbif.org/publisher/' + dwebird['publishingOrgKey']
#dwebird=dwebird[['gbifID', 'datasetKey', 'occurrenceID', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species', 'taxonRank', 'scientificName', 'publishingOrgKey', 'decimalLatitude', 'decimalLongitude', 'eventDate', 'day', 'month', 'year', 'basisOfRecord', 'collectionCode', 'recordedBy', 'countryCode', 'stateProvince','locality']]
dwebird['occurrenceRemarks']='País publicador: CO'




# load GBIF repatriated data
dwgbif = pd.read_table('C:\\Cifras_Colombia\\2020\\Repatriacion\\GBIF\\Repatriacion\\ArchivosFinales\\GBIFrepatriacion2020_ipt.txt')
dwgbif['gbifID']=dwgbif['occurrenceID']
# Add columns to make the GBIF API call later on the script
dwgbif['datasetKey']=dwgbif['datasetID']
dwgbif['datasetKey']=dwgbif.datasetKey.replace({'https://www.gbif.org/dataset/':''}, regex=True)
dwgbif['publishingOrgKey']=dwgbif['institutionID']
dwgbif['publishingOrgKey'] = dwgbif.publishingOrgKey.replace({'https://www.gbif.org/publisher/':''}, regex=True)




# load Xeno-Canto repatriated data
dwXeno = pd.read_table('C:\\Cifras_Colombia\\2020\\Repatriacion\\Xeno-canto\\XenoCantorepatriacion2020_ipt.txt')
dwXeno['gbifID']=dwXeno['occurrenceID']
# Add columns to make the GBIF API call later on the script
dwXeno['datasetKey']=dwXeno['datasetID']
dwXeno['datasetKey']=dwXeno.datasetKey.replace({'https://www.gbif.org/dataset/':''}, regex=True)
dwXeno['publishingOrgKey']=dwXeno['institutionID']
dwXeno['publishingOrgKey'] = dwXeno.publishingOrgKey.replace({'https://www.gbif.org/publisher/':''}, regex=True)




# load iNat repatriated data
dwInat = pd.read_table('C:\\Cifras_Colombia\\2020\\Repatriacion\\INAT\\INATrepatriacion2020_ipt.txt')
dwInat['gbifID']=dwInat['occurrenceID']
# Add columns to make the GBIF API call later on the script
dwInat['datasetKey']=dwInat['datasetID']
dwInat['datasetKey']=dwInat.datasetKey.replace({'https://www.gbif.org/dataset/':''}, regex=True)
dwInat['publishingOrgKey']=dwInat['institutionID']
dwInat['publishingOrgKey'] = dwInat.publishingOrgKey.replace({'https://www.gbif.org/publisher/':''}, regex=True)




# concatenate (add rows) working elements with eBird 
dwme=pd.concat([dwm,dwebird,dwgbif,dwXeno,dwInat], sort=False)
#STOP!!!!, don't run in batch. Remove the unnecesary variables or your PC will COLAPSE.(dwm,dwebird,dwgbif,dwXeno,dwInat) 
#Reorganize the elements and remove unnecesary  fromt the dataset
dwme=dwme[['gbifID', 'occurrenceID', 'basisOfRecord', 'institutionCode', 'collectionCode', 'catalogNumber', 'type', 'institutionID', 'datasetID', 'occurrenceRemarks', 'recordedBy', 'individualCount', 'sex', 'eventID', 'samplingProtocol', 'samplingEffort', 'eventDate', 'year', 'month', 'day', 'habitat', 'continent', 'waterBody', 'country', 'countryCode', 'stateProvince', 'county', 'municipality', 'locality', 'minimumElevationInMeters', 'maximumElevationInMeters', 'minimumDepthInMeters', 'maximumDepthInMeters', 'locationRemarks', 'decimalLatitude', 'decimalLongitude', 'geodeticDatum', 'coordinateUncertaintyInMeters', 'taxonID', 'scientificName', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'specificEpithet', 'infraspecificEpithet', 'taxonRank', 'scientificNameAuthorship', 'taxonRemarks', 'identifiedBy', 'dateIdentified', 'bibliographicCitation', 'previousIdentifications', 'datasetKey', 'publishingOrgKey']]




#Load thematic information. Species checklist with iformation about CITES, endangered categories, etc.
checklistCol = pd.read_table('C:\\Cifras_Colombia\\2020\\Listas\\taxonomicTematicChecklist_Col_species.txt')
#remove unnecesary columns
checklistCol=checklistCol.drop(['scientificNameOrg','kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'specificEpithet', 'infraspecificEpithet', 'taxonRank', 'taxonID'], axis=1)
#Cross main dataframe with thematic information
dwme.rename(columns={'taxonRemarks': 'species'}, inplace=True)
dwme.rename(columns={'lista': 'taxoTematicChecklist'}, inplace=True)
checklistCol.rename(columns={'scientificName': 'species'}, inplace=True)
dwme_t=pd.merge(left=dwme,right=checklistCol, how= 'left', left_on='species', right_on='species')



#Geographic spatial join. Get stateProvince and county according to DANE MGN
#llenar coordenadas vacías con 0's
dwme_t[['decimalLatitude', 'decimalLongitude']] = dwme_t[['decimalLatitude','decimalLongitude']].fillna(value=0)
dwme_t['Coordinates'] = list(zip(dwme_t.decimalLongitude, dwme_t.decimalLatitude))
dwme_t['Coordinates'] = dwme_t['Coordinates'].apply(Point)
dwme_g = gpd.GeoDataFrame(dwme_t, geometry='Coordinates')
dwme_g.crs = {'init' :'epsg:4326'}
#Importar archivo shapefile (revisar la codificación de la capa y el sistema de referencia) preferiblemente que esté ya en WGS84, sino hay que realizar una conversión para que funcione el spatialJoin
colombiaMGN =gpd.read_file('C:\\ValidacionGeografica_SiB-QGIS\\MGN_MPIO_POLITICO.shp', encoding = "utf8")
dwme_g = gpd.sjoin(dwme_g, colombiaMGN, how="left")
dwme_g = dwme_g.drop(['DPTO_CCDGO', 'MPIO_CCDGO', 'MPIO_CNMBR', 'MPIO_CRSLC', 'MPIO_NAREA', 'MPIO_CCNCT', 'MPIO_NANO', 'DPTO_CNMBR', 'Shape_Leng', 'Shape_Area','Coordinates','index_right'], axis=1)
dwme_g.rename(columns={'suggestedS': 'Departamento-ubicacionCoordenada'}, inplace=True)
dwme_g.rename(columns={'suggestedC': 'Municipio-ubicacionCoordenada'}, inplace=True)





#Call the GBIF API to get organization and dataset information
dwme_dk=dwme_g[["gbifID","datasetKey"]]# this is the dataset
df=dwme_dk.drop_duplicates('datasetKey',inplace=False) # work with unique values
#Define the gbif API call for datasetKey
def call_gbif(row):#configure the API call
    try:
        url = "http://api.gbif.org/v1/dataset/"+ str(row['datasetKey'])#define the URL with the column for the call
        
        response = (requests.get(url).text)
        response_json = json.loads(response)
        time.sleep(0.25)
        return response_json
    
    except Exception as e:
        raise e

df['API_response'] = df.apply(call_gbif,axis=1)#aply the call
norm_dk = json_normalize(df['API_response']) #convert the respone in a table
dk = norm_dk[['key','license','doi','title','created','logoUrl','type']]#define the elements you need
dk.rename(columns={'title': 'datasetName'}, inplace=True)#rename columns
dk.rename(columns={'type': 'dataType'}, inplace=True)#rename columns
dk.rename(columns={'key': 'datasetKey'}, inplace=True)#rename columns



#Call the GBIF API to get organization and dataset information
dwme_ok=dwme_g[["gbifID","publishingOrgKey"]]
dfo=dwme_ok.drop_duplicates('publishingOrgKey',inplace=False)
#Define the gbif API call for publishingOrgKey
def call_gbif(row):
    try:
        url = "http://api.gbif.org/v1/organization/"+ str(row['publishingOrgKey'])
        
        response = (requests.get(url).text)
        response_json = json.loads(response)
        time.sleep(0.25)
        return response_json
    
    except Exception as e:
        raise e



dfo['API_response'] = dfo.apply(call_gbif,axis=1)
norm_ok = json_normalize(dfo['API_response'])
ok = norm_ok[['key','title']]
ok.rename(columns={'title': 'organization'}, inplace=True)
ok.rename(columns={'key': 'publishingOrgKey'}, inplace=True)



#merge API call's to dataset
dwc_co=pd.merge(dwme_g,dk,on='datasetKey')
dwc_co=pd.merge(dwc_co,ok,on='publishingOrgKey')

dwc_co_clist=dwc_co[dwc_co.dataType == 'CHECKLIST']
dwc_co_meta=dwc_co[dwc_co.dataType == 'METADATA']


#Remove data type Checklist (occurrence extention) and Metadata (No idea what is doing here)
dwc_co=dwc_co[dwc_co.dataType != 'CHECKLIST']
dwc_co=dwc_co[dwc_co.dataType != 'METADATA']


dwc_co=dwc_co[['gbifID', 'organization', 'occurrenceID', 'basisOfRecord', 'institutionID', 'institutionCode', 'collectionCode', 'catalogNumber', 'type', 'license', 'doi', 'datasetID', 'datasetName', 'dataType', 'occurrenceRemarks', 'recordedBy', 'individualCount', 'sex', 'eventID', 'samplingProtocol', 'samplingEffort', 'eventDate', 'year', 'month', 'day', 'habitat', 'continent', 'waterBody', 'country', 'countryCode', 'stateProvince', 'county', 'municipality', 'locality', 'minimumElevationInMeters', 'maximumElevationInMeters', 'minimumDepthInMeters', 'maximumDepthInMeters', 'locationRemarks', 'decimalLatitude', 'decimalLongitude', 'geodeticDatum', 'coordinateUncertaintyInMeters', 'taxonID', 'scientificName', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'specificEpithet', 'infraspecificEpithet', 'taxonRank', 'scientificNameAuthorship', 'species', 'identifiedBy', 'dateIdentified', 'bibliographicCitation', 'previousIdentifications', 'datasetKey', 'publishingOrgKey', 'lista', 'threatStatus_UICN', 'isInvasive_mads', 'appendixCITES', 'threatStatus_MADS', 'exotic', 'isInvasive_griis', 'endemic', 'migratory', 'Municipio-ubicacionCoordenada', 'Departamento-ubicacionCoordenada', 'created', 'logoUrl',]]



#Export the final dataset
dwc_co.to_csv('C:\\Cifras_Colombia\\2019\\DatosTrimestrales\\dwc_co_2019T4.csv', sep="\t", encoding = "utf8")

#IT'S DONE!!!!








