# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 15:14:59 2020

@author: ricardo.ortiz
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Jan 24 17:21:37 2020

@author: ricardo.ortiz
"""

#Dowload repatriated data from GBIF in the next URL: 
#https://www.gbif.org/occurrence/search?country=CO&repatriated=true&advanced=1
#Download both the SIMPLE (interpreted and simplify data in csv archive) and DARWIN CORE ARCHIVE (interpreted complete data in txt archive)
#Unzip the archives and procede to execute the next script. Verify the location paths of the data before run.


#Import necesary packages to process the data
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point

# Load GBIF interpreted data set
dwi = pd.read_table("C:\\Cifras_Colombia\\2019\\2019-IV\\DescargaRepatriados20200127\\0000001-200127164407474.csv", encoding = "utf8", sep= "\t", quoting=3, 
                    usecols = ['gbifID', 'datasetKey', 'occurrenceID', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species', 'infraspecificEpithet', 'taxonRank', 'scientificName', 'verbatimScientificName', 'verbatimScientificNameAuthorship', 'countryCode', 'individualCount', 'publishingOrgKey', 'decimalLatitude', 'decimalLongitude', 'coordinateUncertaintyInMeters', 'elevation', 'depth', 'eventDate', 'day', 'month', 'year', 'taxonKey', 'speciesKey', 'basisOfRecord', 'institutionCode', 'collectionCode', 'catalogNumber', 'recordNumber', 'identifiedBy', 'dateIdentified', 'recordedBy'])

# Load occurrece-DwCA 
dwo = pd.read_table("C:\\Cifras_Colombia\\2019\\2019-IV\\DescargaRepatriados20200127\\occurrence.txt", encoding = "utf8", sep= "\t", quoting=3,
                    usecols = ['gbifID', 'type','habitat', 'sex', 'samplingProtocol', 'samplingEffort', 'sampleSizeValue', 'sampleSizeUnit', 'continent', 'waterBody', 'islandGroup', 'island', 'stateProvince', 'county', 'municipality', 'locality', 'publishingCountry', 'repatriated'])

#merge dwi dwo
dwm=pd.merge(dwi,dwo,on='gbifID')
#After this you have to remove dwi and dwo from memory... or your computer maybe colapse

#Subset the repatriated data. Do that only if you load all the country data and not just the repatriated data.
dwr=dwm[dwm.publishingCountry != 'CO']
#Remove dwm from memory



#Repatriated dataset estructure
#Add columns
dwr['institutionID'] = 'https://www.gbif.org/publisher/' + dwr['publishingOrgKey'].astype(str)
dwr['bibliographicCitation'] = 'https://www.gbif.org/occurrence/' + dwr['gbifID'].astype(str)
dwr['datasetID'] = 'https://www.gbif.org/dataset/' + dwr['datasetKey'].astype(str)
dwr['geodeticDatum']='forced to WGS84'
dwr['taxonID'] = 'https://www.gbif.org/species/' + dwr['taxonKey'].astype(str)
dwr['continent']=dwr['continent'].fillna('SA')
dwr['country']='Colombia'
dwr['verbatimLocality']=dwr['continent'].astype(str)+ ' | '+dwr['countryCode'].astype(str)+ ' | '+dwr['stateProvince'].astype(str)+ ' | '+dwr['county'].astype(str)+ ' | '+dwr['municipality'].astype(str)+ ' | '+dwr['locality'].astype(str)
dwr['previousIdentifications']=dwr['verbatimScientificName'].astype(str)+ ' '+dwr['verbatimScientificNameAuthorship'].astype(str)
dwr['minimumElevationInMeters']=dwr['elevation'].astype(str)
dwr['maximumElevationInMeters']=dwr['elevation'].astype(str)
dwr['minimumDepthInMeters']=dwr['depth'].astype(str)
dwr['maximumDepthInMeters']=dwr['depth'].astype(str)
dwr['occurrenceRemarks']='País publicador: '+dwr['publishingCountry'].astype(str)
#Split the species element to various elements. The second split (species 2) is taken as specificEpithet (Verify!!!!)
dwr['species1'],dwr['species2'],dwr['species3'],dwr['species4'] = dwr['species'].str.split(' ', 4).str
dwr = dwr.drop(['species1','species3','species4'], axis=1)
dwr.rename(columns={'species2': 'specificEpithet'}, inplace=True)
#Rename scientificName (gbif interpreted with authorship) as acceptedNameUsage
dwr.rename(columns={'scientificName': 'acceptedNameUsage'}, inplace=True)



#Change values according to controled vocabulary
dwr['basisOfRecord']=dwr['basisOfRecord'].replace('HUMAN_OBSERVATION', 'HumanObservation').replace('PRESERVED_SPECIMEN', 'PreservedSpecimen').replace('MACHINE_OBSERVATION', 'MachineObservation').replace('FOSSIL_SPECIMEN', 'FossilSpecimen').replace('MATERIAL_SAMPLE', 'MaterialSample').replace('LIVING_SPECIMEN', 'LivingSpecimen').replace('OBSERVATION', 'HumanObservation').replace('LITERATURE', 'Occurrence').replace('UNKNOWN', 'Occurrence')
dwr['taxonRank']=dwr['taxonRank'].replace('SPECIES', 'Especie').replace('SUBSPECIES', 'Subespecie').replace('GENUS', 'Género').replace('FAMILY', 'Familia').replace('ORDER', 'Orden').replace('CLASS', 'Clase').replace('PHYLUM', 'Filo').replace('KINGDOM', 'Reino').replace('FORM', 'Forma').replace('VARIETY', 'Variedad').replace('UNRANKED', '')
dwr['institutionCode']=dwr['institutionCode'].replace('iNaturalist', 'iNaturalist.org')
dwr.loc[dwr.datasetID == 'https://www.gbif.org/dataset/b1047888-ae52-4179-9dd5-5448ea342a24', 'institutionCode'] = "Xeno-canto Foundation for Nature Sounds"
#Standarize and translate the Type element. Verify the unique values in Type in case you need to add more lines to the scipt ;)
dwr['type'].unique() # See the results in console
#Make changes
dwr['type']=dwr['type'].replace('PhysicalObject', 'Objeto físico').replace('Image', 'Imagen estática').replace('Event', 'Evento').replace('Physical Object', 'Objeto físico').replace('collection', 'Objeto físico').replace('Collection', 'Objeto físico').replace('Sound', 'Sonido').replace('text', 'Texto').replace('Text', 'Texto').replace('StillImage', 'Imagen estática').replace('http://purl.org/dc/dcmitype/PhysicalObject', 'Objeto físico').replace('http://purl.org/dc/dcmitype/StillImage', 'Imagen estática').replace('Dataset', '').replace('event', 'Evento').replace('Colletion', 'Objeto físico').replace('Physical object', 'Objeto físico').replace('physicalObject', 'Objeto físico').replace('Preserved Specimen [en]', 'Objeto físico').replace('physical object', 'Objeto físico')




#Create a scientificName element without Authorship based in the taxonRank value
dwr.loc[dwr.taxonRank == 'Reino', 'scientificName'] = dwr['kingdom'].astype(str)
dwr.loc[dwr.taxonRank == 'Filo', 'scientificName'] = dwr['phylum'].astype(str)
dwr.loc[dwr.taxonRank == 'Clase', 'scientificName'] = dwr['class'].astype(str)
dwr.loc[dwr.taxonRank == 'Orden', 'scientificName'] = dwr['order'].astype(str)
dwr.loc[dwr.taxonRank == 'Familia', 'scientificName'] = dwr['family'].astype(str)
dwr.loc[dwr.taxonRank == 'Género', 'scientificName'] = dwr['genus'].astype(str)
dwr.loc[dwr.taxonRank == 'Especie', 'scientificName'] = dwr['species'].astype(str)
dwr.loc[dwr.taxonRank == 'Subespecie', 'scientificName'] = dwr['species'].astype(str)+' '+dwr['infraspecificEpithet'].astype(str)
dwr.loc[dwr.taxonRank == 'Variedad', 'scientificName'] = dwr['species'].astype(str)+' var. '+dwr['infraspecificEpithet'].astype(str)
dwr.loc[dwr.taxonRank == 'Forma', 'scientificName'] = dwr['species'].astype(str)+' f. '+dwr['infraspecificEpithet'].astype(str)


#Keep species element like a taxonRemark
dwr.rename(columns={'species': 'taxonRemarks'}, inplace=True)
#Rename column gbifID as occurrenceID
dwr.rename(columns={'occurrenceID': 'otherCatalogNumbers'}, inplace=True)
#Rename column gbifID as occurrenceID
dwr.rename(columns={'gbifID': 'occurrenceID'}, inplace=True)


#Organize date
dwr['eventDate'],dwr['ToDrop']= dwr['eventDate'].str.split('T', 2).str
dwr = dwr.drop(['ToDrop'], axis=1)



#Geographic spatial join. Get stateProvince and county according to cartograhpy 1:25000 (2014)
dwr_g = dwr
#llenar coordenadas vacías con 0's
dwr_g[['decimalLatitude', 'decimalLongitude']] = dwr_g[['decimalLatitude','decimalLongitude']].fillna(value=0)
dwr_g['Coordinates'] = list(zip(dwr_g.decimalLongitude, dwr_g.decimalLatitude))
dwr_g['Coordinates'] = dwr_g['Coordinates'].apply(Point)
dwr_g = gpd.GeoDataFrame(dwr_g, geometry='Coordinates')
dwr_g.crs = {'init' :'epsg:4326'}

#Importar archivo shapefile (revisar la codificación de la capa y el sistema de referencia) preferiblemente que esté ya en WGS84, sino hay que realizar una conversión para que funcione el spatialJoin
colombia25 =gpd.read_file('C:\\Cifras_Colombia\\Capas\\Municipal_Colombia_25K.shp', encoding = "utf8")
dwr_g = gpd.sjoin(dwr_g, colombia25, how="left")
dwr_g = dwr_g.drop(['Area', 'index_right','COD_DANE_D','COD_DANE_M','CONCAT','categoria','COD_DANE'], axis=1)
dwr_g.rename(columns={'Municipio': 'Municipio-ubicacionCoordenada'}, inplace=True)
dwr_g.rename(columns={'Departamen': 'Departamento-ubicacionCoordenada'}, inplace=True)



#Remove innecesary elements
dwr_g = dwr_g[['type', 'bibliographicCitation', 'institutionID', 'datasetID', 'institutionCode','collectionCode', 'basisOfRecord', 'occurrenceID', 'catalogNumber','otherCatalogNumbers', 'occurrenceRemarks', 'recordedBy', 'individualCount', 'samplingProtocol', 'samplingEffort', 'eventDate', 'year', 'month', 'day', 'habitat', 'continent', 'waterBody', 'island', 'country','countryCode', 'stateProvince', 'county','municipality', 'locality', 'verbatimLocality', 'minimumElevationInMeters', 'maximumElevationInMeters', 'minimumDepthInMeters', 'maximumDepthInMeters', 'decimalLatitude', 'decimalLongitude', 'geodeticDatum', 'coordinateUncertaintyInMeters', 'taxonID', 'previousIdentifications', 'identifiedBy', 'dateIdentified', 'scientificName','acceptedNameUsage', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'specificEpithet', 'infraspecificEpithet', 'taxonRank', 'taxonRemarks', 'Departamento-ubicacionCoordenada', 'Municipio-ubicacionCoordenada']]
#Remove empty elements
dwr_g = dwr_g.dropna(axis=1, how='all')




#subset eBird from datasets
dwebird=dwr_g[dwr_g.datasetID == 'https://www.gbif.org/dataset/4fa7b334-ce0d-4e88-aaae-2e0c138d049e']
dwebird = dwebird.dropna(axis=1, how='all') #Remove empty elements
#subset Xeno-Canto from dataset
dwxeno=dwr_g[dwr_g.datasetID == 'https://www.gbif.org/dataset/b1047888-ae52-4179-9dd5-5448ea342a24']
dwxeno = dwxeno.dropna(axis=1, how='all')
#subset iNat from dataset
dwinat=dwr_g[dwr_g.datasetID == 'https://www.gbif.org/dataset/50c9509d-22c7-4a22-a47d-8c48425ef4a7']
dwinat = dwinat.dropna(axis=1, how='all')
dwinat.fillna('', inplace=True)
#remove repatriated from datasets
dwgbif=dwr_g[dwr_g.datasetID != 'https://www.gbif.org/dataset/b1047888-ae52-4179-9dd5-5448ea342a24']
dwgbif=dwgbif[dwgbif.datasetID != 'https://www.gbif.org/dataset/50c9509d-22c7-4a22-a47d-8c48425ef4a7']
dwgbif = dwgbif.dropna(axis=1, how='all')


#Export the results
dwxeno.to_csv('C:\\Cifras_Colombia\\2020\\Repatriacion\\Xeno-Canto\\dwxeno.txt', sep="\t", encoding = "utf8")
dwinat.to_csv('C:\\Cifras_Colombia\\2020\\Repatriacion\\iNat\\dwinat.txt', sep="\t", encoding = "utf8")
dwgbif.to_csv('C:\\Cifras_Colombia\\2020\\Repatriacion\\GBIF\\dwgbif_v2_20200127.txt', sep="\t", encoding = "utf8")


dwi = pd.read_table("C:\\Cifras_Colombia\\2020\\Repatriacion\\GBIF\\dwgbif.txt", encoding = "utf8", sep= "\t", quoting=3)
dwr_g=dwi
