# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 15:09:04 2019
Last Modified May 10 10:00:00 2019 #Se añadió un segmento para consultas geográficas a partir de polígonos

@authors: ricardo.ortiz 
"""

#Consultas

# Este script permite resolver consultas taxonómicas y geográficas de los datos publicados en el SiB Colombia de forma rápida

#1. Consulta Taxonómica
#2. Consulta por departamento
#3. Consulta por munucipio
#4. Consulta Geográfica
#5. Consulta por fecha de evento
#6. Consulta por DOI


#Importar paquetes necesarios
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point


#Ingresar datos según el tipo de consulta

d= 'Guaviare' #Constula por departamentos
m= 'San Pedro de Los Milagros', 'San Pedro de Los Milagros', 'San Pedro' #Consulta por municipios
f= 2019 #Consultas por año del evento
doi= "10.15472/49lw4s" #Consulta por doi



#Cargar el dataset con los datos de Colombia previamente espacializados e intersecados con el MGN del DANE
dwc_co = pd.read_csv('C:\\Cifras_Colombia\\2019\\DatosTrimestrales\\dwc_co_2019T4.txt', encoding = "utf8", sep="\t")


# Las consultas pueden ser anidadas, taxonomía, geografía (departamento), geografía (municipio). Siga los pasos viendo los comentarios


#---------------------------------------------1. Consulta taxonómica ---------------------------------------------------------------------------
# Si las consultas son taxonomicas, usar #TAXONOMÍA para generar búsqueda de grupos biológicos
# Tener en cuenta si la consulta incluye varios grupos biológicos 
# ej. Kingdom: Plantae | class: Aves, Mammalia, Reptilia. Deben generarse subsets y luego concatenarlos:

taxo1 = dwc_co[(dwc_co['family'] =='Piperaceae')]
taxo2 = dwc_co[dwc_co['class'].isin(['Amphibia','Reptilia','Aves'])]
taxo3 = dwc_co[dwc_co['scientificName'].isin(['Myrmecophaga tridactyla', 'Tamandua mexicana', 'Tamandua tetradactyla', 'Cyclopes didactylus'])]
taxo=pd.concat([taxo1,taxo2,taxo3], sort=False)



#---------------------------------------------2. Consulta por departamento -----------------------------------------------------------------------

## Consultas CON requerimientos taxonómicos

    ### Un solo departamento
dpto_g= taxo[(taxo['Departamento-ubicacionCoordenada']== d)]
dpto_t = taxo[(taxo['stateProvince']== d)]
dpto=pd.concat([dpto_g,dpto_t], sort=False)
dpto=dpto.drop_duplicates('gbifID',inplace=False)

    ###Varios departamentos
dpto_g = taxo[taxo['Departamento-ubicacionCoordenada'].isin(d)]
dpto_t= taxo[taxo['stateProvince'].isin(d)]
dpto=pd.concat([dpto_g,dpto_t], sort=False)
dpto=dpto.drop_duplicates('gbifID',inplace=False)


## Consultas SIN requerimientos taxonómicos

    ### Un solo departamento
dpto_t = dwc_co[(dwc_co['stateProvince']== d)]
dpto_g = dwc_co[(dwc_co['Departamento-ubicacionCoordenada']== d)]
dpto=pd.concat([dpto_g,dpto_t], sort=False)
dpto=dpto.drop_duplicates('gbifID',inplace=False)

    ### Varios departamentos
dpto_t = dwc_co[dwc_co['stateProvince'].isin(d)]
dpto_g = dwc_co[dwc_co['Departamento-ubicacionCoordenada'].isin(d)]
dpto=pd.concat([dpto_g,dpto_t], sort=False)
dpto=dpto.drop_duplicates('gbifID',inplace=False)


#---------------------------------------------3. Consulta por municipios -----------------------------------------------------------------------

## Para evitar errores de sinónimos entre municipios, generar un subset de departamentos y luego seleccionar los municipios de interés

    ### Un solo municipio
mpios_g = dpto[(dpto['Municipio-ubicacionCoordenada']== m)]
mpios_t = dpto[(dpto['county']== m)]
mpios=pd.concat([mpios_g,mpios_t], sort=False)
mpios=mpios.drop_duplicates('gbifID',inplace=False)

    ### Varios municipios
mpios_g = dpto[dpto['Municipio-ubicacionCoordenada'].isin(m)]
mpios_t = dpto[dpto['county'].isin(m)]
mpios=pd.concat([mpios_g,mpios_t], sort=False)
mpios=mpios.drop_duplicates('gbifID',inplace=False)


#---------------------------------------------4. Consulta geográfica - Cruce Polígono-----------------------------------------------------------

# Para optimizar el cruce geográfico, de ser posible, realice prefiltros de departamento y municipio. Según haya sido el caso seleccione
# una de las siguientes opciones.

#Nacional
geo = dwc_co
#Filtro Departamental
geo = dpto
#Filtro Municipal
geo = mpios

geo[['decimalLatitude', 'decimalLongitude']] = geo[['decimalLatitude','decimalLongitude']].fillna(value=0)
geo['Coordinates'] = list(zip(geo.decimalLongitude, geo.decimalLatitude))
geo['Coordinates'] = geo['Coordinates'].apply(Point)
geo = gpd.GeoDataFrame(geo, geometry='Coordinates')
geo.crs = {'init' :'epsg:4326'}

#El script contien el código para un ejemplo de cruce con dos diferetes polígonos, para mas polígonos copie y pegue cambiando la ubicación del archivo
#Importar archivo shapefile (revisar la codificación de la capa y el sistema de referencia) preferiblemente que esté ya en WGS84, sino hay que realizar una conversión para que funcione el spatialJoin
polygon1 =gpd.read_file("C:\\Users\\ricardo.ortiz\\Documents\\Consultas_2020\\LB_SerraniaPerija\\Shapes\\Serrania_2PNN_1DMI.shp", encoding = "utf8")

#Realizar cruces Geo primer polígono
geo_j = gpd.sjoin(geo, polygon1, how="left", op="intersects")
#Filtre por nombre columna los elementos que no hayan quedado vacíos, ese será el resultado del cruce geográfico. Remplace NombreColumna por el nombre del alguna columna del shapefile
geo_r= geo_j[geo_j.NombreColumna.notnull()]


#---------------------------------------------5. Consulta por año del evento-----------------------------------------------------------

eventD = dwc_co[(dwc_co['year']== f)]

#---------------------------------------------6. Consulta por año del evento-----------------------------------------------------------

DOI=dwc_co[dwc_co['doi'].isin(doi)]


#--------------------------------------------------------------------------------------------------------------------------------------


#TIPO DE RESPUESTA

#Elija según corresponda
Resultado = taxo #1 
Resultado = dpto #2
Resultado = mpios #3
Resultado = geo_r # 4
Resultado = eventD #5
Resultado = DOI # 6



#A: Simplificada
Resultado = Resultado[['gbifID','occurrenceID', 'organization', 'datasetName', 'doi', 'license', 'collectionCode', 'catalogNumber', 'basisOfRecord', 'scientificName', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'species', 'infraspecificEpithet', 'taxonRank', 'eventDate', 'day', 'month', 'year', 'recordedBy', 'countryCode', 'stateProvince', 'county', 'municipality', 'locality', 'minimumElevationInMeters','maximumElevationInMeters', 'decimalLatitude', 'decimalLongitude', 'coordinateUncertaintyInMeters', 'Departamento-ubicacionCoordenada', 'Municipio-ubicacionCoordenada', 'created']]

#B: Completa (Inlcuye información de especies endémicas, amenazadas, exóticas y migratorias)
Resultado = Resultado[['gbifID', 'organization', 'occurrenceID', 'basisOfRecord', 'institutionID', 'institutionCode', 'collectionCode', 'catalogNumber', 'type', 'license', 'doi', 'datasetID', 'datasetName', 'dataType', 'occurrenceRemarks', 'recordedBy', 'individualCount', 'sex', 'eventID', 'samplingProtocol', 'samplingEffort', 'eventDate', 'year', 'month', 'day', 'habitat', 'continent', 'waterBody', 'country', 'countryCode', 'stateProvince', 'county', 'municipality', 'locality', 'minimumElevationInMeters', 'maximumElevationInMeters', 'minimumDepthInMeters', 'maximumDepthInMeters', 'locationRemarks', 'decimalLatitude', 'decimalLongitude', 'geodeticDatum', 'coordinateUncertaintyInMeters', 'taxonID', 'scientificName', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'specificEpithet', 'infraspecificEpithet', 'taxonRank', 'scientificNameAuthorship', 'species', 'identifiedBy', 'dateIdentified', 'bibliographicCitation', 'previousIdentifications', 'datasetKey', 'publishingOrgKey', 'taxoTematicChecklist', 'threatStatus_UICN', 'isInvasive_mads', 'appendixCITES', 'threatStatus_MADS', 'exotic', 'isInvasive_griis', 'endemic', 'migratory', 'Municipio-ubicacionCoordenada', 'Departamento-ubicacionCoordenada', 'created', 'logoUrl']]

#C: Específica (Solo incluir campos según sean solicitados)
Resultado = Resultado[['gbifID', 'organization', 'occurrenceID', 'basisOfRecord', 'institutionID', 'institutionCode', 'collectionCode', 'catalogNumber', 'type', 'license', 'doi', 'datasetID', 'datasetName', 'dataType', 'occurrenceRemarks', 'recordedBy', 'individualCount', 'sex', 'eventID', 'samplingProtocol', 'samplingEffort', 'eventDate', 'year', 'month', 'day', 'habitat', 'continent', 'waterBody', 'country', 'countryCode', 'stateProvince', 'county', 'municipality', 'locality', 'minimumElevationInMeters', 'maximumElevationInMeters', 'minimumDepthInMeters', 'maximumDepthInMeters', 'locationRemarks', 'decimalLatitude', 'decimalLongitude', 'geodeticDatum', 'coordinateUncertaintyInMeters', 'taxonID', 'scientificName', 'kingdom', 'phylum', 'class', 'order', 'family', 'genus', 'specificEpithet', 'infraspecificEpithet', 'taxonRank', 'scientificNameAuthorship', 'species', 'identifiedBy', 'dateIdentified', 'bibliographicCitation', 'previousIdentifications', 'datasetKey', 'publishingOrgKey', 'taxoTematicChecklist', 'threatStatus_UICN', 'isInvasive_mads', 'appendixCITES', 'threatStatus_MADS', 'exotic', 'isInvasive_griis', 'endemic', 'migratory', 'Municipio-ubicacionCoordenada', 'Departamento-ubicacionCoordenada', 'created', 'logoUrl']]

#Nombre del archivo de salida
x="FCDS_datosGuaviare.txt"



#EXPORTAR RESULTADOS

Resultado.to_csv('C:\\Consultas\\Respuestas\\' + x, sep="\t", encoding = "utf8")




